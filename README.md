# PostCodes.IO #

Is a helper class for the Postcodes.IO API...

Add the following to your projects composer to bring in the code:

    {
        "require": {
            "lcherone/PostCodesIO": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:lcherone/postcodes.io.git"
            }
        ]
    }


###Usage###

    <?php
    require 'vendor/autoload.php';

    use PostCodesIO\PostCodesIO;

    $postcodes = new PostCodesIO;

    /**
        Array
        (
            [status] => 200
            [result] => Array
                (
                    [postcode] => B2 2UF
                    [quality] => 1
                    [eastings] => 407279
                    [northings] => 288677
                    [country] => England
                    [nhs_ha] => West Midlands
                    [longitude] => -1.8942139293579
                    [latitude] => 52.495956327562
                    [parliamentary_constituency] => Birmingham, Ladywood
                    [european_electoral_region] => West Midlands
                    [primary_care_trust] => Heart of Birmingham Teaching
                    [region] => West Midlands
                    [lsoa] => Birmingham 050A
                    [msoa] => Birmingham 050
                    [nuts] =>
                    [incode] => 2UF
                    [outcode] => B2
                    [admin_district] => Birmingham
                    [parish] =>
                    [admin_county] =>
                    [admin_ward] => Aston
                    [ccg] => NHS Sandwell and West Birmingham
                    [codes] => Array
                        (
                            [admin_district] => E08000025
                            [admin_county] => E99999999
                            [admin_ward] => E05001179
                            [parish] => E43000179
                            [ccg] => E38000144
                        )

                )

        )
     */
    print_r(json_decode($postcodes->random_postcode(), true));
    ?>