<?php
namespace PostCodesIO;

use Requests;

/**
 * Postcodes.io API
 * @link       http://postcodes.io/
 * @author     Lawrence Cherone <lawrence@opensauce.systems>
 * @version    1.00
 */

class PostCodesIO
{

    private static $postcode = null;
    private static $json = false;
    private static $header = array('Accept' => 'application/json');

    const API = 'https://api.postcodes.io';

    /**
    * regex check to see if passed string is a valid UK postcode format
    * @param string $postcode
    * @scope private
    * @return bool
    */
    private static function isPostcode($postcode = "")
    {
        if (preg_match("/(^[A-Z]{1,2}[0-9R][0-9A-Z]?[\s]?[0-9][ABD-HJLNP-UW-Z]{2}$)/i", $postcode) ||
            preg_match("/(^[A-Z]{1,2}[0-9R][0-9A-Z]$)/i", $postcode)) {
            return true;
        }

        return false;
    }

    /**
    * Filtering function that will check the passed values to the class methods are valid postcode
    * @param mixed $postcodes
    * @scope private
    * @return bool
    */
    private static function filterPostcode($postcodes)
    {
        if (is_array($postcodes)) {
            foreach ($postcodes as $key => $postcode) {
                $postcode = strtoupper(str_replace(' ', null, $postcode));

                if (self::isPostcode($postcode)) {
                    $postcodes[$key] = true;
                } else {
                    $postcodes[$key] = false;
                }
            }

            self::$postcode = $postcodes;

            return true;
        } else {
            self::$postcode = urlencode(strtoupper(str_replace(' ', null, $postcodes)));

            if (self::isPostcode(self::$postcode)) {
                return true;
            } else {
                return true; //force true
            }
        }
    }

    /**
    * Lookup a postcode
    * @param string $postcode - The postcode string
    * @scope public
    * @return mixed [object or false]
    */
    public static function lookup($postcode)
    {
        //if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/postcodes/'.$postcode,
                self::$header
            );
            return self::$json->body;
        //}

        return false;
    }

    /**
    * Lookup multiple postcodes
    * @param array $postcodes - Array containing a bunch of postcodes
    */
    public static function lookup_multi($postcodes = array())
    {
        if (self::filterPostcode($postcodes)) {
            self::$json = Requests::post(
                self::API.'/postcodes',
                self::$header,
                json_encode(array('postcodes' => self::$postcode))
            );
            return self::$json->body;
        }
    }

    /**
    * Get nearest postcodes for a given longitude & latitude
    * @param float $log
    * @param float $lat
    */
    public static function lookup_nearest($longitude, $latitude)
    {
        if ((trim($longitude, '0') == (float) $longitude) && (trim($latitude, '0') == (float) $latitude)) {
            self::$json = Requests::get(
                self::API.'/postcodes/lon/'.$longitude.'/lat/'.$latitude,
                self::$header
            );
            return self::$json->body;
        }
        return false;
    }

    /**
    * Get nearest postcodes for a given longitude & latitude
    * @param float $log
    * @param float $lat
    */
    public static function lookup_nearest_postcodes($postcode = "")
    {
        if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/postcodes/'.self::$postcode.'/nearest',
                self::$header
            );
            return self::$json->body;
        }
        return false;
    }

    /**
    * Bulk reverse geocoding
    * @param array $postcodes - Array containing a bunch of postcodes
    */
    public static function reverse_geocode($postcode = array())
    {
        if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/postcodes/'.self::$postcode.'/nearest',
                self::$header
            );
            return self::$json->body;
        }
        return false;
    }

    /**
    * Get a random postcode
    */
    public static function random_postcode()
    {
        self::$json = Requests::get(
            self::API.'/random/postcodes',
            self::$header
        );
        print_r(self::$json);
        return self::$json->body;
    }

    /**
    * Validate postcode
    * @param string $postcodes
    */
    public static function validate_postcode($postcode = "")
    {
        if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/postcodes/'.self::$postcode.'/validate',
                self::$header
            );
            return self::$json->body;
        }
        return false;
    }

    /**
    * Auto complete
    * @param string $postcodes
    */
    public static function autocomplete_postcode($postcode = "")
    {
        //if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/postcodes/'.$postcode.'/autocomplete',
                self::$header
            );
            return self::$json->body;
        //}
        return false;
    }

    /**
    * Query postcode
    * @param string $postcodes
    */
    public static function query_postcode($postcode = "")
    {
        //if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/postcodes?q='.self::$postcode,
                self::$header
            );
            return self::$json->body;
        //}
        return false;
    }

    /**
    * Lookup outward code
    * @param string $postcodes
    */
    public static function lookup_outward($postcode = "")
    {
        if (self::filterPostcode($postcode)) {
            self::$json = Requests::get(
                self::API.'/outcodes/'.self::$postcode,
                self::$header
            );
            return self::$json->body;
        }
        return false;
    }

}
